Project Setup
===================

.. contents:: Contents

Objectives
-------------

* Understand how to setup a Xcode project
* Create a simple Hello World application

Create Xcode Project
-----------------------

Whenever you are starting development of any iOS application, 
it is required that you have knowledge of how to setup a project in Xcode.  Xcode is the free development tool
provided by Apple to deliver applications.  If you are running Lion or Mountain Lion you can just download Xcode
from the app store. If you are still on Snow Leopard you can go to Apple's developer site to download it. After it 
is downloaded, we can start by launching it.

.. image:: images/xcode_front.png

After it is launched we are greeted with a start wizard page.  Then we click on "Create a new Xcode Project".
The next thing you have to pick is the type of project.  Here we will select a "Single View Application" 
and click "Next".

.. image:: images/new_project_1.png

After we have moved to the next panel, we need to fill out information about the project.  
First we set the Project Name.  Then set the Organization Name to be either the name of 
your company or just your name.  Now define the Organization Identifier as the inverse of 
a domain the organization owns.  

.. image:: images/project_info.png

.. image:: images/set_location.png


Creating Hello Application
----------------------------


