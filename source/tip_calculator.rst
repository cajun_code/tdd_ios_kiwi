Tip Calculator
================

.. contents:: Contents

Problem Statement
--------------------

::

  Given that I want to calculate the amount of the tip
  When I enter $50 at 17% tip
  Then I should see 8.5 for the tip amount 
  And $58.50 for the total amount
  
I know this looks weird.  The format of this text is called Gerken.  This format is 
used by business to describe a process they want to preform. 


Project Setup
------------------

To start the project, we will start with a basic Xcode project.  
Create a new Single View iOS Application with the name TipCalculator.  
Setup the project with Unit Tests, ARC and Stroyboards checked.  

Now that we have the basic project setup we need to add some ruby components 
to help with the project. By default Mac has ruby version 1.8.7 installed.

Install and Configure Bundler
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The first component we need is Bundler.  This will manage ruby dependencies for us.  
To install:

.. code-block:: bash
  
  $ sudo gem install bundler
  
Bundler gives us commands and tools to manage the installed ruby libraries.  
To initialize bundler run the following command in the project directory.

.. code-block:: bash
  
  $ bundle init

This command creates a file called a Gemfile.  

.. literalinclude:: ../code/TipCalculator/Gemfile
    :language: ruby
    :lines: 1-4

Let's add cocoapods and frank to the Gemfile.

.. literalinclude:: ../code/TipCalculator/Gemfile
    :language: ruby
    :lines: 5-6

Now we can run:

.. code-block:: bash
  
  $ bundle install
  
This install the ruby gems and all their dependencies.  Now we are ready to setup iOS dependencies.

Configure Cocoapods
^^^^^^^^^^^^^^^^^^^^^^^

Cocoapods is a tool for managing iOS application dependencies. Since we installed it with the bundle install 
command, all we need to do is run:

.. code-block:: bash
  
  $ pod setup
  
Next step is to write the Podfile:

.. literalinclude:: ../code/TipCalculator/Podfile
    :linenos:
    :language: ruby

What this will do is download the code for the library Kiwi and setup to be used by the testing target. 
To start the process we simply run: 

.. code-block:: bash
  
  $ pod install

Configure Frank
^^^^^^^^^^^^^^^^^^^^^

Frank is a testing tool that runs at the UI Level.  To install Frank:

.. code-block:: bash
  
  $ frank setup

To build the frankified version of the application run:

.. code-block:: bash
  
  $ frank build_and_launch
  
This compiles the app and includes a small web server to work with the app.  if you run  'frank inspect' it will launch a web browser and allow you to inspect the app. Now we can run our first cucumber test.
  
.. code-block:: bash

  $ export APP_BUNDLE_PATH=./Frank/frankified_build/./Frankified.app
  $ cucumber Frank/features/my_first.feature 

Start with the Model
----------------------

Let's start by creating a test.  In Xcode, select "File" -> "New" -> "File..." to open up the File template dialog.  Select "Kiwi" under iOS, then select the Kiwi template.  Next you can enter "Calculator" for the name of the class we want to test. The last step is to decide where the files go on the file system. I would put them into the Application test directory.  

Open the CalculatorSpec.m file to edit 

.. literalinclude:: ../code/TipCalculator/TipCalculatorTests/CalculatorSpec.m
    :linenos:
    :language: objc
    :lines: 1-5,36-38

Now we need to create the Calender.
