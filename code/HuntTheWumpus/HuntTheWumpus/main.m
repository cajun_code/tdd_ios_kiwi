//
//  main.m
//  HuntTheWumpus
//
//  Created by Allan Davis on 9/24/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Game.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        //NSLog(@"Hello, World!");
        Game * game = [[Game alloc]init];
        [game startLoop];
        
    }
    return 0;
}

