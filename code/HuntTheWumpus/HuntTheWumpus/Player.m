//
//  Player.m
//  HuntTheWumpus
//
//  Created by Allan Davis on 9/30/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import "Player.h"

@implementation Player

- (NSString *) preformAction:(NSString* ) action{
    return @"hello";
}

- (NSDictionary *) pronouns{
    NSDictionary *pronouns = nil;
    if( [self.sex isEqualToString:@"M"] || [self.sex isEqualToString:@"m"]){
        pronouns = @{@"possessive" : @"his", @"personal": @"he"};
    }else{
        pronouns = @{@"possessive" : @"hers", @"personal": @"she"};
    }
    return pronouns;
}
@end
