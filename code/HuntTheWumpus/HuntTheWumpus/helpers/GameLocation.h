//
//  GameLocation.h
//  HuntTheWumpus
//
//  Created by Allan Davis on 10/1/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#ifndef HuntTheWumpus_GameLocation_h
#define HuntTheWumpus_GameLocation_h
struct GamePoint{
    int x;
    int y;
};

#endif
