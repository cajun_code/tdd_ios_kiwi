//
//  Input.m
//  HuntTheWumpus
//
//  Created by Allan Davis on 9/30/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import "ADInput.h"

@implementation ADInput

+ (NSString *) getString{
    char data[100];
    scanf("%s", data);
    return [NSString stringWithUTF8String:data];
}
+ (char) getChar{
    char data;
    scanf("%c", &data);
    return data;
}
+ (int) getInt{
    int data;
    scanf("%d", &data);
    return data;
}
+ (float) getFloat;{
    float data;
    scanf("%f", &data);
    return data;
}
+ (void) clearScreen{
    system("clear");
}

@end
