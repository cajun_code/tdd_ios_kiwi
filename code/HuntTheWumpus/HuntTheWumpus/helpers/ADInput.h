//
//  Input.h
//  HuntTheWumpus
//
//  Created by Allan Davis on 9/30/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADInput : NSObject

+ (NSString *) getString;
+ (char) getChar;
+ (int) getInt;
+ (float) getFloat;
+ (void) clearScreen;

@end
