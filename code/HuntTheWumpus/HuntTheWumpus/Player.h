//
//  Player.h
//  HuntTheWumpus
//
//  Created by Allan Davis on 9/30/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Entity.h"

@interface Player : NSObject
@property (strong) NSString *name;
@property (strong) NSString *sex;
@property (readonly) NSDictionary *pronouns;


- (NSString *) preformAction:(NSString* ) action;
@end
