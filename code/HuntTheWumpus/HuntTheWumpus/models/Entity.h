//
//  Entity.h
//  HuntTheWumpus
//
//  Created by Allan Davis on 9/25/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameLocation.h"

@interface Entity : NSObject
@property struct GamePoint *location;
@property int hitPoints;
@property int lifePoints;
@end
