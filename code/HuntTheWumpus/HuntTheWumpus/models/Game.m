//
//  Game.m
//  HuntTheWumpus
//
//  Created by Allan Davis on 9/24/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import "Game.h"
#import "ADInput.h"
@implementation Game



- (void) startLoop{
    Game * game = [[Game alloc]init];
    game.gameRunning = YES;
    //setup game
    while (game.gameRunning) {
        // update objects
        //draw the object
        //gather input
        [game exitGame];
    }
}

- (void) exitGame{
    printf("Type exit to quit. \n");
    NSString *input = [ADInput getString];
    puts(input.UTF8String);
    if ([input isEqualToString:@"exit"]) {
        self.gameRunning = NO;
    }

}

- (void) setup{
    NSDictionary *pronouns = self.player.pronouns;
    NSString *story = @"";
    
    story = [story stringByAppendingFormat: @"After a great hunt, %@ returned to the village. ", self.player.name];
    story = [story stringByAppendingFormat: @"To %@ dismay, %@ finds the village ravaged. ", [pronouns objectForKey:@"possessive"],[pronouns objectForKey:@"personal"]];
    story = [story stringByAppendingFormat: @"Bodies of %@ friends lay everywhere, battered and broken. ", [pronouns objectForKey:@"possessive"]];
    story = [story stringByAppendingFormat: @"%@ continued to survey the damage, only to find huge tracks of what looked like a giant wumpus. ", [pronouns objectForKey:@"personal"]];
}
@end
