//
//  Game.h
//  HuntTheWumpus
//
//  Created by Allan Davis on 9/24/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

@interface Game : NSObject
@property BOOL gameRunning;
@property Player *player;

- (void)startLoop;


@end
