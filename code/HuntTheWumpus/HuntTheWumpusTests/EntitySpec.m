#import <Kiwi/Kiwi.h>
#import "Entity.h"

SPEC_BEGIN(EntitySpec)
    describe(@"Entity", ^{
        __block Entity *entity;
        beforeAll(^{
            entity = [[Entity alloc] init];
        });
        xit(@"should keep track of it's x Coordinate", ^{
            entity.location->x = 1;
            [[theValue(entity.location->x) should] equal:theValue(1)];
        });
        xit(@"should keep track of it's y Coordinate", ^{
            entity.location->y = 1;
            [[theValue(entity.location->y) should] equal:theValue(1)];
        });
        xit(@"should keep trakc of it's hit points", ^{
            entity.hitPoints = 20;
            [[theValue(entity.hitPoints) should] equal: theValue(20)];
        });
        xit(@"should keep track of it's life points", ^{
            entity.lifePoints = 500;
            [[theValue(entity.lifePoints) should] equal: theValue(500)];
        });
    });
SPEC_END