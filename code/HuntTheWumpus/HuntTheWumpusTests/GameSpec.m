#import <Kiwi/Kiwi.h>
#import "Game.h"

SPEC_BEGIN(GameSpec)
    describe(@"Game", ^{
        __block Game *game;
        beforeEach(^{
            game = [[Game alloc]init];
        });
        context(@"gameloop", ^{
            xit(@"should make sure the screen is clear before drawing", ^{
                
                [[game should] receive:@selector(clearTheScreen)];
                [game startLoop];
            });
            xit(@"should gather the input", ^{
                
            });
            xit(@"should update the game data", ^{
                
            });
            xit(@"should draw the game", ^{});
            xit(@"should allow breaking of the game loop", ^{});
        });
        
    });
SPEC_END