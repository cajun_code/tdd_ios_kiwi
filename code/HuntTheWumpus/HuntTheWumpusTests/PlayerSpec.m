#import <Kiwi/Kiwi.h>
#import "Player.h"

SPEC_BEGIN(PlayerSpec)
    describe(@"Player", ^{
        __block Player *player = nil;
        beforeEach(^{
            player = [[Player alloc]init];
        });
        xit(@"should have a name", ^{
            player.name = @"Allan";
            [[player.name should] equal: @"Allan"];
        });
        pending_(@"should keep track of it hit points", ^{});
        
        describe(@"preformAction", ^{
            xit(@"Should respond with I can't do this if it cannot respond to the action", ^{
                NSString * result = [player preformAction: @"bark"];
                [[result should] equal: @"I can't do that"];
            });
        });
    });
SPEC_END