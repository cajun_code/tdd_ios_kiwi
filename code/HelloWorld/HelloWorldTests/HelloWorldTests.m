//
//  HelloWorldTests.m
//  HelloWorldTests
//
//  Created by Allan Davis on 9/9/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import "HelloWorldTests.h"

@implementation HelloWorldTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in HelloWorldTests");
}

@end
