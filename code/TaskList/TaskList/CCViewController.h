//
//  CCViewController.h
//  TaskList
//
//  Created by Allan Davis on 10/30/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCViewController : UITableViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSArray *tasks;

@end
