//
//  Task+CRUD.m
//  TaskList
//
//  Created by Allan Davis on 11/1/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import "Task+CRUD.h"

@implementation Task (CRUD)

+ (NSFetchRequest *) createFetchRequestWithContext: (NSManagedObjectContext *)context{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Task" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
//    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"category.name = %@", category.name];
//    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    return fetchRequest;
}
+ (NSArray *) allTaskFromContext:(NSManagedObjectContext *)context{
    NSFetchRequest *fetchRequest = [self createFetchRequestWithContext:context];
    NSError *error;
    return [context executeFetchRequest:fetchRequest error:&error];
    
}
+ (Task *) taskWithName:(NSString*)name fromContext:(NSManagedObjectContext *)context{
    Task *task = [NSEntityDescription insertNewObjectForEntityForName:@"Task" inManagedObjectContext:context];
    task.name = name;
    NSError *error = nil;
    [context save:&error];
    return task;
}

- (void)save:(NSError *)error{
    [self.managedObjectContext save: &error];
}

- (void)deleteTask{
    [self.managedObjectContext delete:self];
}

@end
