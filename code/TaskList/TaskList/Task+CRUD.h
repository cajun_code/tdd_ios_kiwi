//
//  Task+CRUD.h
//  TaskList
//
//  Created by Allan Davis on 11/1/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import "Task.h"

@interface Task (CRUD)
+ (NSFetchRequest *) createFetchRequestWithContext: (NSManagedObjectContext *)contex;

+ (NSArray *) allTaskFromContext:(NSManagedObjectContext *)context;

+ (Task *) taskWithName:(NSString*)name fromContext:(NSManagedObjectContext *)context;

- (void)save:(NSError *)error;
- (void)deleteTask;
@end
