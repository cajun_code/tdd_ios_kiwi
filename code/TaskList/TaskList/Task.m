//
//  Task.m
//  TaskList
//
//  Created by Allan Davis on 11/1/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import "Task.h"
#import "Category.h"


@implementation Task

@dynamic done;
@dynamic name;
@dynamic category;

@end
