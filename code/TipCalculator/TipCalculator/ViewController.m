//
//  ViewController.m
//  TipCalculator
//
//  Created by Allan Davis on 9/6/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.calc = [[Calculator alloc] init];
    self.percentageValue.text = [NSString stringWithFormat:@"%.0f%%", self.percentage.value * 100];
}

- (void)viewDidUnload
{
    [self setBill:nil];
    [self setPercentage:nil];
    [self setPercentageValue:nil];
    [self setTipAmount:nil];
    [self setTotalAmount:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)sliderChaged:(UISlider *)sender {
    self.percentageValue.text = [NSString stringWithFormat:@"%.0f%%", self.percentage.value * 100];
    self.calc.billAmount = [self.bill.text floatValue];
    self.calc.tipPercentage = self.percentage.value;
    [self.calc processTip];
    self.tipAmount.text = [NSString stringWithFormat:@"%.2f", self.calc.tipAmount];
    self.totalAmount.text = [NSString stringWithFormat:@"%.2f", self.calc.totalAmount];
}
@end
