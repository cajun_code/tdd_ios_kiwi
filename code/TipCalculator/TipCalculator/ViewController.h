//
//  ViewController.h
//  TipCalculator
//
//  Created by Allan Davis on 9/6/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Calculator.h"

@interface ViewController : UIViewController
- (IBAction)sliderChaged:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *bill;
@property (weak, nonatomic) IBOutlet UISlider *percentage;
@property (weak, nonatomic) IBOutlet UILabel *percentageValue;
@property (weak, nonatomic) IBOutlet UILabel *tipAmount;
@property (weak, nonatomic) IBOutlet UILabel *totalAmount;
@property (strong, nonatomic) Calculator *calc;
@end
