//
//  Calculator.m
//  TipCalculator
//
//  Created by Allan Davis on 9/15/12.
//  Copyright (c) 2012 Cajun Code. All rights reserved.
//

#import "Calculator.h"

@implementation Calculator
-(void)processTip{
    self.tipAmount = self.billAmount * self.tipPercentage;
    self.totalAmount = self.billAmount + self.tipAmount;
}
@end
